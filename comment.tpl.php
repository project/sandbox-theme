  <div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
    <?php if ($picture) {
    print $picture;
  } ?>
<h3 class="comment-title"><?php print $title; ?></h3><?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
    <div class="comment-date"><?php print $submitted; ?></div>
    <div class="comment-content"><?php print $content; ?></div>
    <div class="comment-meta">&raquo; <?php print $links; ?></div>
  </div>
