  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h2 class="entry-title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <div class="entry-date"><?php print $submitted?></div>
    <div class="entry-content"><?php print $content?></div>
    <div class="entry-meta">
	<div class="taxonomy"><?php print $terms?></div>
    	<?php if ($links) { ?> <div class="links">&raquo; <?php print $links?></div><?php }; ?>
    </div>
  </div>
