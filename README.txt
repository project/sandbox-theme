Drupal port of Sandbox themes
Jun, 2007

port by: Hari Prasad Nadig
              http://hpnadig.net

Thanks to the wonderful folks who did the Sandbox themes. It is a great previlege to be porting it to Drupal. 

Note:
Not everything is the same. I had to change couple of class names to suit Drupal. For a detailed information on changes, Read Drupal-port-README.txt.

Sandbox themes Web page:
http://www.plaintxt.org/themes/sandbox/

