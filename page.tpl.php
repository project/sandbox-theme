<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<div id="wrapper" class="hfeed">
	<div id="header">
 	<?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
 <?php if ($site_name) { ?><h1 id="blog-title"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
		<?php if ($site_slogan) { ?> <div id="blog-description"><?php print $site_slogan ?></div><?php } ?>
	</div><!--  #header -->

    <div id="access">
		<div class="skip-link"><a href="#content" title="Skip navigation to the content">Skip to content</a></div>
    </div><!-- #access -->

    <div id="container">
     <div id="content">

			<div id="nav-above" class="navigation">
      				<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
      				<?php print $search_box ?>
			</div>

        <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
      </div>

      <div id="nav-below" class="navigation">
	<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
      </div>

      </div><!-- #content -->
     </div><!-- #container -->
   
    <?php if ($sidebar_left) { ?>	
	<div id="primary" class="sidebar">
      <?php print $sidebar_left ?>
	</div>
    <?php } ?>
      
    <?php if ($sidebar_right) { ?>	
	<div id="secondary" class="sidebar">
      <?php print $sidebar_right ?>
    </div><?php } ?>


<div id="footer">
  <?php print $footer_message ?>
</div>
<?php print $closure ?>

</div>
</body>
</html>
